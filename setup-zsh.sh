sudo apt-get install zsh -y

mkdir -p "$HOME/.zsh"
git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
curl -sL git.io/antigen > ~/.zsh/antigen.zsh
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
            ~/.zsh/zsh-syntax-highlighting
