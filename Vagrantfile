Vagrant.configure('2') do |config|

  config.vm.box = 'fasmat/ubuntu2004-desktop'
  config.vm.box_version = '20.0813.1'

  # config.vm.box = 'chenhan/ubuntu-desktop-20.04'
  # config.vm.box_version = '20200424'
  config.vbguest.auto_update = false

  config.vm.hostname = 'ubuntu-python-box'

  config.vm.provider 'virtualbox' do |vb|
    vb.name = 'Ubuntu 20.04 Python Dev Box'
    vb.gui = true
    vb.memory = '8192'
    vb.cpus = 2
    vb.customize ['modifyvm', :id, '--vram', '256']
    vb.customize ['modifyvm', :id, '--graphicscontroller', 'vboxsvga']
    # vb.customize ['modifyvm', :id, '--accelerate3d', 'on']
    vb.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
    vb.customize ["modifyvm", :id, "--draganddrop", "bidirectional"]
  end

  config.vm.provision 'initial-setup', type: 'shell', keep_color: true,
    privileged: false do |s|
      s.path = './init.sh'
  end

  config.vm.synced_folder './code', '/home/vagrant/code'

  config.vm.provision 'copy-gitconfig', type: 'file', source: './gitconfig',
    destination: '~/.gitconfig'
  config.vm.provision 'copy-zshrc', type: 'file', source: './zshrc',
    destination: '~/.zshrc'
  config.vm.provision 'copy-tmux-conf', type: 'file', source: './tmux.conf',
    destination: '~/.tmux.conf'

  config.vm.provision 'copy-pylintrc', type: 'file', source: './python/pylintrc',
    destination: '~/code/.pylintrc'
  # used by `black`
  config.vm.provision 'copy-pyproject', type: 'file', source: './python/pyproject.toml',
    destination: '~/code/pyproject.toml'
  config.vm.provision 'copy-isort', type: 'file', source: './python/isort.cfg',
    destination: '~/code/.isort.cfg'
  config.vm.provision 'copy-vscode-settings', type: 'file', source: './settings.json',
    destination: '~/.config/Code/User/settings.json'

  config.vm.provision 'copy-coc-nvim', type: 'file', source: './nvim/coc.vim',
    destination: '~/.config/nvim/coc.vim'
  config.vm.provision 'copy-slime-nvim', type: 'file', source: './nvim/slime.vim',
    destination: '~/.config/nvim/slime.vim'
  config.vm.provision 'copy-init-nvim', type: 'file', source: './nvim/init.vim',
    destination: '~/.config/nvim/init.vim'

  config.vm.provision 'configure-gnome', type: 'shell', keep_color: true,
    privileged: false do |s|
    s.path = './configure-gnome.sh'
  end

  config.vm.provision 'change-shell', type: 'shell', keep_color: true,
      privileged: false do |s|
    s.inline = 'sudo apt-get autoremove -y && sudo chsh -s /bin/zsh vagrant'
    s.reboot = true
  end

end
