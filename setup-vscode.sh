# snap install is taking too long for some reason
# sudo snap install --classic code

wget 'https://go.microsoft.com/fwlink/?LinkID=760868' -qO code.deb
sudo dpkg -i ./code.deb && rm -f ./code.deb
code --install-extension vscode-icons-team.vscode-icons
code --install-extension ms-python.python
code --install-extension wesbos.theme-cobalt2
code --install-extension ms-python.vscode-pylance
