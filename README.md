# How to use the repository

## Setup

* Install Vagrant & Virtualbox for your platform
* Run `vagrant up`. Let all the installations complete, let the VM reboot once.
* Press Host key(right-ctrl) + F to go full-screen, make sure you have 4K
  resolution selected

## Details

This minimal Python dev box has the following software intalled:

* zsh, its config file, plugins like pure prompt, antigen, zsh plugins(git, pip, zsh-autosuggestions, k, fasd,
  exa, fzf, lf, delta, common-aliases, zsh-syntax-highlighting)
* gitconfig file
* vs code, its config file and shell file to setup its extensions, neovim and its config file
* python 3, pip3, pylint, pytest, black - all will work with vs code(can optionally enable pylance;  check settings.json)
* neovim nightly build, Vim Plug plugin manager, config file
* python 3, conda, pyright, pytest, etc. - all will work with neovim
* tmux and its configuration
* basic gnome configuration

## Manual steps

* Open ~/.gitconfig and change git user name & email fields if necessary
* CAPSLOCK is mapped to ESC in configure-gnome.sh, change it if you need
* Only nvm & node are installed(as node is needed for some tools like vim plugins)
  Feel free to uncomment lines in setup-node.sh and install npm packages of your choice
* You have to manually edit the version numbers of packages in install scripts
  if you want their latest versions

## Python

Check /vagrant/python/README.md

## TODO

* Create run/debug configuration for python projects for vs code using launch.json
* Evaluate if formulahendry.code-runner extension is useful, install it if it
  is

## Lubuntu issues

* How to use Super+Left for tiling instead of Super+Ctrl+Left while Super is
  still available for searching
* How to disable prompt when exiting the terminal
    Use a terminal like alacritty
* How to disable the prompt when doing reboot/shutdown from menu
    Run reboot/shutdown command from the terminal instead
