call plug#begin('~/.config/nvim/plugged')

:set shortmess-=F " avoid suppressing error messages from plugin
:set nocompatible " no need to be compatible with very old version of vim

" https://arisweedler.medium.com/tab-settings-in-vim-1ea0863c5990
:set expandtab " replace tabs with spaces
:set tabstop=4 " \t is these many spaces
:set softtabstop=4 " tab key press or a backspace press is these many spaces"
:set shiftwidth=4 " level of indentation is these many spaces

:set number " show line numbers
" :set nohlsearch " don't show highlights for search

" highlight all matches while searching, to move cursor to next/previous match
" while searching, etc.
:set incsearch

:set hidden " hide buffer instead of closing when opening a new file
:set undofile " persist undo information in a file

" ignore case when search unless enabled via option or if at least one upper case
" letter present in search string
:set ignorecase
:set smartcase

" auto install all listed plugins in init.vim when nvim is started
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
" autocmd BufWritePre * %s/\s\+$//e

" remove highlights after search is complete
Plug 'romainl/vim-cool'

" show trailing whitespace & strip them on file save
Plug 'ntpeters/vim-better-whitespace'
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:current_line_whitespace_disabled_soft=1

Plug 'vim-airline/vim-airline'
Plug 'mhinz/vim-signify'
" Plug 'metakirby5/codi.vim'
Plug 'jpalardy/vim-slime', { 'for': ['python'] }
let g:slime_target = "tmux"
Plug 'hanschen/vim-ipython-cell', { 'for': 'python' }
" map <Leader>s to start IPython
nnoremap <Leader>s :SlimeSend1 ipython<CR>
" map <Leader>r to run script
nnoremap <Leader>r :IPythonCellRun<CR>
" map <Leader>c to execute the current cell
nnoremap <Leader>c :IPythonCellExecuteCellVerbose<CR>
" map <Leader>C to execute the current cell and jump to the next cell
nnoremap <Leader>C :IPythonCellExecuteCellVerboseJump<CR>

" Plug 'liuchengxu/vim-which-key'
" " Create map to add keys to
" let g:which_key_map =  {}
" let g:which_key_map['h'] = [ '<C-W>s'                     , 'split below']
" nnoremap <silent> <leader> :silent :WhichKey '<Space>'<CR>
" autocmd! FileType which_key
" autocmd  FileType which_key set laststatus=0 noshowmode noruler
"   \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
" call which_key#register('<Space>', "g:which_key_map")

Plug 'jiangmiao/auto-pairs' " insert brackets and quotes as pairs
Plug 'tpope/vim-commentary'  " commenting shortcuts

"
" code completion, go to declaration/definition, hover tooltips, linting,
" formatting etc. using python-language-server from Microsoft
" need to install python-language-server package
" pip install python-language-server
" conda install -c anaconda python-language-server

Plug 'prabirshrestha/vim-lsp'
" Plug 'mattn/vim-lsp-settings'

" Plugin works only with Neovim 0.5 or later
" Plug 'neovim/nvim-lspconfig'
" lua <<EOF
" require'lspconfig'.pyls.setup{}
" EOF
" set completeopt-=preview

Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
imap <c-space> <Plug>(asyncomplete_force_refresh)

if executable('pyls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
        \ 'workspace_config': {'pyls': {'plugins': {'pydocstyle': {'enabled': v:true}, 'pyls-mypy': {'enabled': v:true}}}}
        \ })
endif

" Use LSP omni-completion in Python files.
" autocmd Filetype python setlocal omnifunc=v:lua.vim.lsp.omnifunc
function! s:on_lsp_buffer_enabled() abort
    " setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    " nmap <buffer> gr <plug>(lsp-references)
    " nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

Plug 'psf/black', { 'branch': 'stable' }
autocmd BufWritePre *.py execute ':Black'
let g:black_virtualenv='~/code/.venv'

" gruvbox theme
Plug 'morhetz/gruvbox'
autocmd vimenter * ++nested colorscheme gruvbox

call plug#end()
