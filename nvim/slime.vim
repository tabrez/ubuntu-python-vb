
" vim-slime configuration
let g:slime_target = "tmux"

" vim-ipython-cell configuration
" map <Leader>s to start IPython
nnoremap <Leader>s :SlimeSend1 ipython<CR>
" map <Leader>r to run script
nnoremap <Leader>r :IPythonCellRun<CR>
" map <Leader>c to execute the current cell
nnoremap <Leader>c :IPythonCellExecuteCellVerbose<CR>
" map <Leader>C to execute the current cell and jump to the next cell
nnoremap <Leader>C :IPythonCellExecuteCellVerboseJump<CR>

" Plug 'metakirby5/codi.vim' " BUG: unable to disable codi in a buffer
