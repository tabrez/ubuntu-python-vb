call plug#begin('~/.config/nvim/plugged')

" Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/vim-lsp'
" Plug 'piec/vim-lsp-clangd'
" Plug 'prabirshrestha/asyncomplete-lsp.vim'
" Plug 'keremc/asyncomplete-clang.vim'
Plug 'mattn/vim-lsp-settings'

" vim-lsp configuration
" https://github.com/prabirshrestha/vim-lsp
function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    " setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" asyncomplete configuration
" https://github.com/prabirshrestha/asyncomplete.vim
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

" ENTER key alwasys inserts a new line
" inoremap <expr> <cr> pumvisible() ? asyncomplete#close_popup() . "\<cr>" : "\<cr>"

imap <c-space> <Plug>(asyncomplete_force_refresh)

" disable auto-pop of preview menu
" let g:asyncomplete_auto_popup = 0

" function! s:check_back_space() abort
"     let col = col('.') - 1
"     return !col || getline('.')[col - 1]  =~ '\s'
" endfunction

" inoremap <silent><expr> <TAB>
"   \ pumvisible() ? "\<C-n>" :
"   \ <SID>check_back_space() ? "\<TAB>" :
"   \ asyncomplete#force_refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" allow modifying the completeopt variable, or it will
" be overridden all the time
let g:asyncomplete_auto_completeopt = 0

set completeopt=menuone,noinsert,noselect,preview

" auto-close preview window when completion is done
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" asyncomplete-clang configuration
" https://github.com/keremc/asyncomplete-clang.vim
" autocmd User asyncomplete_setup call asyncomplete#register_source(
"     \ asyncomplete#sources#clang#get_source_options({
"     \     'config': {
"     \         'clang_path': '/usr/bin/clang-10',
"     \         'clang_args': {
"     \             'default': ['-I/opt/llvm/include'],
"     \             'cpp': ['-std=c++11', '-I/opt/llvm/include']
"     \         }
"     \     }
"     \ }))

" autocmd User asyncomplete_setup call asyncomplete#register_source(
"     \ asyncomplete#sources#clang#get_source_options())

call plug#end()
