" https://github.com/scrapbird/dotfiles/tree/master/config/nvim
" https://codeinthehole.com/tips/vim-text-objects/
" https://www.vimfromscratch.com/articles/vim-for-python/

" map leader key to space
let mapleader = " "

call plug#begin('~/.config/nvim/plugged')

" Plug 'tpope/vim-surround' " mappings to easily delete, change and add surroundings in pairs
Plug 'tpope/vim-commentary'  " commenting shortcuts
Plug 'justinmk/vim-sneak' " Jump to any location specified by two characteres s{char1}{char2}
" Plug 'easymotion/vim-easymotion' " Jump to any location specified by two characteres s{char1}{char2}
Plug 'romainl/vim-cool' " remove highlights after search is complete
Plug 'itchyny/lightline.vim' " minimal status bar
Plug 'mhinz/vim-signify' " show signs for git changes
Plug 'ryanoasis/vim-devicons' " show file type icons in fzf etc.
" Plug 'preservim/tagbar' " show summary of class/method/etc. names in a sidebar
Plug 'sheerun/vim-polyglot' " language pack for vim
" Plug 'wellle/targets.vim' " additional text objects
Plug 'jiangmiao/auto-pairs' " insert or delete pairs of brackets
Plug 'ctrlpvim/ctrlp.vim' " fuzzy find files
Plug 'michaeljsmith/vim-indent-object' " vim text object for code at same indent level

" Plug 'junegunn/vim-easy-align'

" Code completion, linting etc.
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" evaluate python code; read vim-slime documentation to know how to connect it
" to a tmux pane (e.g. select 'default' & '%1' when prompted by :SlimeConfig)
Plug 'jpalardy/vim-slime', { 'for': ['python'] }
Plug 'hanschen/vim-ipython-cell', { 'for': 'python' }

" gruvbox theme
Plug 'morhetz/gruvbox'
" dracula theme
Plug 'dracula/vim', { 'as': 'dracula' }
" One-dark theme
Plug 'joshdick/onedark.vim'

" markdown support
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" Vagrantfile support
" Plug 'hashivim/vim-vagrant'

call plug#end()

set nocompatible " no need to be compatible with vi
set shortmess-=F " don't give the file info when editing a file
set shortmess+=c " don't give 'ins-completion-menu' messages
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" set formatoptions-=cro " don't add comments automatically
" set cmdheight=2 " Give more space for displaying messages
set clipboard+=unnamedplus " share vim clipboard with system clipboard
set nobackup
set nowritebackup
set hidden " hide buffer instead of closing when opening a new file
set undofile " persist undo information in a file
set backspace=indent,eol,start " make backspace behave properly in insert mode

set ttyfast
set lazyredraw
set ttimeout
set ttimeoutlen=50
set showcmd
set wildmenu
set wildmode=longest:full,full
set nowrap " disable soft wrap for lines
set cursorline " highlight current line

" https://arisweedler.medium.com/tab-settings-in-vim-1ea0863c5990
set expandtab " replace tabs with spaces
set tabstop=2 " \t is these many spaces
set softtabstop=2 " tab key press or a backspace press is these many spaces
set shiftwidth=2 " level of indentation is these many spaces
filetype plugin indent on
" autocmd Filetype python setlocal ts=2 sw=2 expandtab

set number relativenumber " show line numbers

" highlight all matches while searching, to move cursor to next/previous match
" while searching, etc.
" set incsearch
" ignore case when searching unless enabled via option or if at least one upper case
" letter present in search string
set ignorecase
set smartcase

set updatetime=300 " Having longer updatetime leads to noticeable delays
set signcolumn=yes

autocmd BufWritePre * %s/\s\+$//e " strip whitespace on file save in all files

" auto install all listed plugins in init.vim when nvim is started
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif

" autocmd vimenter * ++nested colorscheme gruvbox
colorscheme gruvbox

" dracula customisation
" colorscheme dracula
" hi Pmenu ctermfg=NONE ctermbg=236 cterm=NONE guifg=NONE guibg=#64666d gui=NONE
" hi PmenuSel ctermfg=NONE ctermbg=24 cterm=NONE guifg=NONE guibg=#204a87 gui=NONE

" onedark customisation
" let g:onedark_hide_endofbuffer = 1 " hide end-of-buffer filler lines(~)
" let g:lightline = {
"   \ 'colorscheme': 'onedark',
"   \ }
" colorscheme onedark

set background=dark

" handle single quote pair for python f/r/b-strings
au FileType python let b:AutoPairs = AutoPairsDefine({"f'" : "'", "r'" : "'", "b'" : "'"})

let g:sneak#label = 1 " show labels on matching text like ace-jump/easy-motion

let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" Start interactive EasyAlign in visual mode (e.g. vipga)
" xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
" nmap ga <Plug>(EasyAlign)

" nmap <F8> :TagbarToggle<CR>

" keybind to save file in normal
nnoremap <Leader>g :<c-u>update<cr>

" Ctrl-x to save file in normal, visual, insert modes
nnoremap <silent><c-q> :<c-u>q<cr>
vnoremap <silent><c-q> <c-c>:q<cr>gv
inoremap <silent><c-q> <c-o>:q<cr>

" source ~/.config/nvim/coc.vim
source ~/.config/nvim/slime.vim
source ~/.config/nvim/vim-lsp.vim
" source ~/.config/nvim/easymotion.vim
