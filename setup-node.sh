curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
. ~/.nvm/nvm.sh
nvm install --lts

# npm install -g yarn
# npm install -g typescript
# npm install -g eslint
# npm install -g babel-eslint
# npm install -g eslint-config-standard
# npm install -g eslint-config-standard-react
# npm install -g eslint-config-standard-jsx
# npm install -g eslint-plugin-react
# npm install -g eslint-config-prettier
# npm install -g eslint-plugin-prettier
# npm install -g prettier
# npm install ndb node-inspector
# npm install -g fkill vtop tldr
