#!/bin/bash

# set HiDPI scaling factor for 4K resolution
# gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "[{'Gdk/WindowScalingFactor', <2>}]"
gsettings set org.gnome.desktop.interface scaling-factor 2

# configure gnome terminal
GNOME_TERMINAL_PROFILE=`gsettings get org.gnome.Terminal.ProfilesList default | awk -F \' '{print $2}'`
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ audible-bell false
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ use-theme-colors false
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ foreground-color 'rgb(131,148,150)'
gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$GNOME_TERMINAL_PROFILE/ background-color 'rgb(0,43,54)'

# configure gedit
gsettings set org.gnome.gedit.preferences.editor scheme 'solarized-dark'

# configure apps to be shown on favourites bar
gsettings set org.gnome.shell favorite-apps "['ubiquity.desktop', 'org.gnome.Terminal.desktop', 'firefox.desktop', 'code_code.desktop', 'org.gnome.Nautilus.desktop']"

# configure Dark theme
gsettings set org.gnome.desktop.interface gtk-theme Yaru-dark

# configure date & time format
# timedatectl set-ntp 1
gsettings set org.gnome.desktop.interface clock-format 12h
sudo timedatectl set-timezone Asia/Kolkata

# map capslock key to ESC for neovim
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:swapescape']"

# hide dock and make icons smaller
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
# gsettings set org.gnome.shell.extensions.dash-to-dock autohide true
gsettings set org.gnome.shell.extensions.dash-to-dock intellihide true
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 16

# set keyboard layout
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'en')]"
