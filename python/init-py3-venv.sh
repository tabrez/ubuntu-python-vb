apt-cache show python3-venv | grep python3-venv
[ $? -eq 0 ] || sudo apt-get install python3-venv -y

python -m venv .venv
. .venv/bin/activate

python -m pip install wheel pycodestyle pydocstyle pylint mypy isort black
python -m pip install pytest pytest-watch ipython bpython ptpython jupyterlab
python -m pip install python-language-server pynvim pyls-mypy pyls-isort
