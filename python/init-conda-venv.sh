[ -d ~/miniconda2 ] || sh ./setup-conda.sh

# run the following commands to create a new conda environment
# conda create -y -n py39 python=3.9
# conda activate py39

conda install -y -c conda-forge python-language-server pynvim pyls-mypy pyls-isort
conda install -y -c conda-forge pylint pycodestyle pydocstyle mypy isort black
conda install -y -c anaconda pytest pytest-watch ptpython ipython jupyterlab

# use the following for faster installations:
# conda install -c confa-forge mamba -y
# mamba install -c conda-forge python-language-server pynvim pyls-mypy pyls-isort
# mamba install -c conda-forge pylint pycodestyle pydocstyle mypy isort black
# mamba install -c anaconda pytest pytest-watch ptpython ipython jupyterlab
