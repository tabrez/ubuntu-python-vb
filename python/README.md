# Working with Python

## Basic setup

A default python 3 project should already be setup to work with VS Code in
~/code folder.  Above setup is performed using /vagrant/python/setup-python.sh,
look at the source code to know what was done or to customise it and run
it again.

## New project using python 3 venv & pip

Any time you need to create a new Python project, use
/vagrant/python/init-py3-env.sh file.  Feel free to customise the script before
you run it.

```bash
mkdir newproj && cd newproj
sh /vagrant/python/init-py3-env.sh
source .venv/bin/activate
code .
```

## New project using conda

You can create a new conda virtual enviroment:
```bash
sh /vagrant/python/init-conda-venv.sh
```
You need to change the name of the env in /vagrant/python/init-conda-env.sh
before running the script to create each new environment.

You can install conda using /vagrant/python/setup-conda.sh but this is
automatically done by the above script if conda is not found

## Misc

* bpython is a minimal python repl with colored completion menus & syntax
highlighting
* ptpython is better but not as good looking as bpython
* ptipython is the same as ptpython but supports ipython magic commands
* ~/code/config.py has example configuration for ptpython/ptipython

In short, you should use `ptipython` unless you've a reason to use something else.

Use the following command in your python project folder to generate pylintrc
configuration file if you like to customise the rules:

```bash
pylint --generate-rcfile > .pylintrc
```

This file should already be present in the ~/code folder.

Add a [pycodestyle] section to setup.cfg/tox.ini file in your python project folder
to customise pycodestyle rules

Consider adding flake8 package as an alternative linting tool
