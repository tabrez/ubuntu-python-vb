#!/bin/bash

# Manual steps if the script fails with errors:
# 0. (Optional) Setup Ubuntu with zsh, npm, vscode, dotfiles, etc.:
#    wget -qO- http://bit.ly/srtpldf > ~/setup && bash ~/setup web
# 1. Logout, login again and open the shell. Download Miniconda:
#    wget https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
# 2. Install Miniconda:
#    bash ./Miniconda2-latest-Linux-x86_64.sh
# 3. Restart shell/console. Then create new Python 3.x virtual enviroment:
#    conda create -n venv -y
# 4. Switch to venv virtual environment when working on ML:
#    conda activate venv
# 5. Install essential packages:
#    conda install -y -c conda-forge numpy pandas scikit-learn jupyterlab matplotlib ipython
# 6. Run Juptyer Lab:
#    jupyter lab&

# TODO: check if previous command succeeded or not before executing the next steps

if test "$EUID" = 0; then
    echo "run this script as non-root user"
    exit 1
fi

FILE=miniconda2.sh
URL="https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh"
echo "Downloading and Installing Miniconda 2..."
wget "$URL" -qO "/tmp/$FILE" && sh "/tmp/$FILE" -b -f


~/miniconda2/bin/conda init zsh
~/miniconda2/bin/conda init bash
. ~/miniconda2/etc/profile.d/conda.sh

# Run the following commands to create a new conda environment
# conda create -n venv -y
# conda activate venv

echo "Removing downloaded miniconda shell file..."
rm -f "/tmp/$FILE"

echo "If there were errors, run the steps manually; otherwise, run 'jupyter lab&' without the quotes."
