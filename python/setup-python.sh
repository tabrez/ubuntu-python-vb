[ -L /usr/bin/python ] || sudo ln -s /usr/bin/python3 /usr/bin/python
[ -L /usr/bin/pip ] || sudo ln -s /usr/bin/pip3 /usr/bin/pip
sudo apt-get install python3-pip -y
sudo python -m pip install --upgrade pip

cd ~/code
sh /vagrant/python/init-py3-venv.sh
