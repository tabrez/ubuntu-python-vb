#! /bin/sh

# Install NeoVim stable
# https://github.com/neovim/neovim/wiki/Installing-Neovim
# pip install neovim
# sudo apt-get install nvim vim-runtime

# Install NeoVim 0.5
wget https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -qO ~/.local/bin/nvim.appimage
chmod u+x ~/.local/bin/nvim.appimage

# Install vim-plug
# https://github.com/junegunn/vim-plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Tagbar plugin in NeoVim depends on ctags
sudo apt-get install ctags -y
