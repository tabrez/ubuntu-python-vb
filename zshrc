export TERM="xterm-256color"
[ -n "$TMUX" ] && export TERM=tmux-256color
export EDITOR="nvim.appimage"
export VISUAL="nvim.appimage"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# configure minimal shell prompt using pureprompt
# https://github.com/sindresorhus/pure
# Run the following commands from your shell to install pure-prompt
# [ -d $HOME/.zsh ] || mkdir -p "$HOME/.zsh"
# git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
fpath+=~/.zsh/pure
autoload -U promptinit; promptinit
zstyle :prompt:pure:path color green
PURE_PROMPT_SYMBOL="λ"
autoload -U compinit; compinit
prompt pure

# use antigen as the plugin manager - https://github.com/zsh-users/antigen
# Run the following command from your shell to install antigen:
# curl -L git.io/antigen > ~/.zsh/antigen.zsh
ANTIGEN_LOG=~/.antigen/antigen.log
source ~/.zsh/antigen.zsh
antigen use oh-my-zsh

# install zsh plugins
antigen bundle git
# antigen bundle pip
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle MichaelAquilina/zsh-you-should-use
export YSU_MESSAGE_POSITION="after"
export YSU_MODE=ALL
antigen bundle supercrabtree/k
# antigen bundle fasd
antigen bundle common-aliases
# antigen theme dracula/zsh
antigen apply

# Run the following command from your shell:
# git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
#   ~/.zsh/zsh-syntax-highlighting
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.nvm/nvm.sh

alias v="~/.local/bin/nvim.appimage"
alias vt="vim.tiny"
alias c="code"
alias o="xdg-open"
alias fd="fdfind"
alias cat="batcat"
alias tu="tmux -u"
alias l="exa"
alias ll="exa -l"
alias lf="TERM=xterm-256color lf"
alias apts="apt-search.sh"

export PATH="$PATH:/snap/bin:$HOME/.local/bin"
if [ "$TMUX" = "" ]; then
  neofetch
fi

[ -f ~/.local/bin/fasd ] && eval "$(fasd --init auto)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

if [ "$TMUX" = "" ]; then
  tmux -u
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/tabrez/miniconda2/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/tabrez/miniconda2/etc/profile.d/conda.sh" ]; then
        . "/home/tabrez/miniconda2/etc/profile.d/conda.sh"
    else
        export PATH="/home/tabrez/miniconda2/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
