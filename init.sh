#!/bin/sh

DEBIAN_FRONTEND=noninteractive sudo apt-get update -y
DEBIAN_FRONTEND=noninteractive sudo apt-get upgrade -y
DEBIAN_FRONTEND=noninteractive sudo apt-get dist-upgrade -y

sudo apt-get install git trash-cli xclip bat fd-find tmux -y
sudo apt-get install neofetch ctags silversearcher-ag -y

wget https://github.com/BurntSushi/ripgrep/releases/download/12.1.1/ripgrep_12.1.1_amd64.deb -qO /tmp/ripgrep.deb && sudo dpkg -i /tmp/ripgrep.deb && sudo apt-get install -f

[ -d ~/.local/bin ] || mkdir -p ~/.local/bin

wget https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip -qO /tmp/exa.zip
unzip /tmp/exa.zip -d ~/.local/bin/
mv ~/.local/bin/*exa* ~/.local/bin/exa
chmod +x ~/.local/bin/exa

wget https://github.com/clvv/fasd/zipball/1.0.1 -qO /tmp/fasd.zip
unzip /tmp/fasd.zip -d ~/.local/bin/
mv ~/.local/bin/*fasd*/fasd ~/.local/bin/
chmod +x ~/.local/bin/fasd

git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
# following method is simpler but won't get docs etc. installed
# wget https://github.com/junegunn/fzf/releases/download/0.25.0/fzf-0.25.0-linux_amd64.tar.gz -qO /tmp/fzf.tar.gz
# tar zxf /tmp/fzf.tar.gz -C ~/.local/bin/

wget https://download.opensuse.org/repositories/home:/Provessor/xUbuntu_20.04/amd64/lf_16+git20200907.61442f8-1_amd64.deb -qO /tmp/lf.deb
sudo dpkg -i /tmp/lf.deb && sudo apt-get install -f

wget https://github.com/dandavison/delta/releases/download/0.5.0/git-delta_0.5.0_amd64.deb -qO /tmp/git-delta.deb
sudo dpkg -i /tmp/git-delta.deb && sudo apt-get install -f

# alacritty doesn't work in virtual machines?
# https://github.com/alacritty/alacritty/blob/master/INSTALL.md
# sudo add-apt-repository ppa:mmstick76/alacritty -y
# apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3
# sudo apt-get install alacritty -y

# check out the following xterm configuration, consider merging it with our .Xresources
# https://gist.github.com/BrandonIrizarry/4ed01b4090066d41fa136518920184cd
sudo apt-get install xterm -y

sh /vagrant/setup-zsh.sh
sh /vagrant/setup-node.sh
sh /vagrant/setup-nvim.sh
sh /vagrant/python/setup-python.sh
sh /vagrant/configure-gnome.sh
